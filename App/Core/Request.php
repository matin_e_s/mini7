<?php
namespace App\Core;

class Request
{

    public static $referer;
    public static $agent;
    public static $method;
    public static $uri;
    public static $params;

    public function __construct()
    {
        $this->params = $_REQUEST;
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->uri = $_SERVER['REQUEST_URI'];
        $this->agent = $_SERVER['HTTP_USER_AGENT'];
        $this->referer = $_SERVER['HTTP_REFERER'];
    }
}

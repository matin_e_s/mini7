<?php

namespace App\Models;

class Base_Model
{
    
    function __construct()
    {
        global $conn;
        $conn = $medoo;
    }
    protected function creat($data)
    {
        $this->$conn->insert($this->table, $data);
    }
    protected function read($columns, $where)
    {
        $this->$conn->select($this->table, $columns, $where);
    }
    protected function update($data, $where)
    {
        $this->$conn->update($this->table, $data, $where);
    }
    protected function delete($where)
    {
        $this->$conn->delete($this->table, $where);
    }
    protected function query($query)
    {
        $this->$conn->query($query);
    }
    protected function count($where)
    {
        $this->$conn->count($this->table, $where);
    }
}

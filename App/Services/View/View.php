<?php

namespace App\Services\View;

class View
{

    public static function load($view)
    {
        $view_file_path = BASE_PATH . "$view";
        if (file_exists($view_file_path) && is_readable($view_file_path))
            return include "$view_file_path";
    }

    public static function render($view)
    {
        ob_start();
        self::load($view);
        return ob_get_clean();
    }
}

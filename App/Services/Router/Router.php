<?php

namespace App\Services\Router;

use App\Core\Request;

class Router
{
    private static $routes;

    public static function start()
    {
        //get all routes
        $routes = self::get_all_routes();
        //get current uri
        $current_uri = self::current_uri();
        //var_dump($current_uri);

        //check if rout exist
        if (self::route_exists($current_uri)) {
            $request = new Request;

            //check if method exist
            $method_list = self::get_methods();
            if ($request->is_in($method_list)) { } else {
                echo '403 error';
            }
            //check if middleware exist
            if (!has_middleware()) {
                die();
            }
            $middlwareObj = new $middlware;
            //check if controller exist
            $target_str = self::get_target();
            [$controller, $method] = explode("@", $target_str);
            
        } else {
            header('HTTP/1.0 404 Not Found');
            echo 'not exist';
            //View::load(404.php);
            die();
        }
    }

    function get_all_routes()
    {
        return include BASE_PATH . 'routes\web.php';
    }

    function current_uri()
    {
        return str_replace('/MicroFramework/', '', $_SERVER['REQUEST_URI']);
    }

    function route_exists($route)
    {
        $routes = self::get_all_routes();
        return in_array($route, array_keys($routes));
    }
}
